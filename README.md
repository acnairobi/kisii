# Atlassian Kisii CI/CD Demo App
A simple HTML site to demo on deploying to Firebase with Bitbucket Pipelines

## Configuration

Get the configs and explanations on the [Presentation Doc](https://docs.google.com/presentation/d/1ts2nuAFtWcTHLakwxLGpzcD_22USE_vNw_h8260IXYY/edit?usp=sharing)

## Contributer 

[Magak Emmanuel](https://manuel.appslab.co.ke)

## Credits

1. [Atlassian Nairobi](https://ace.atlassian.com/nairobi)

2. [Apps:Lab KE](https://appslab.co.ke)

## Licence 
Licenced under MIT
